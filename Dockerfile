FROM node
FROM mongo

WORKDIR /codeyardapp

COPY package*.json ./

RUN npm install
COPY . .

EXPOSE 4000
CMD [ "node", "server.js" ]