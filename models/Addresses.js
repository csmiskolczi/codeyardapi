import mongoose from 'mongoose';

const appSchema = new mongoose.Schema({
  text:{type:String,required:true},
  user:{type:mongoose.SchemaTypes.ObjectId},
});

const Todo = mongoose.model('Addresses', appSchema);

export default Todo;